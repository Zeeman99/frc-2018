package frc.team4192.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team4192.robot.Constants;
import frc.team4192.robot.commands.drive.StickDrive;

/**
 * This class provides drive functionality to the robot. It uses the two most common forms of standard drive, arcade and tank. Motor ports
 * can be changed in the RobotMap.
 */
public class DriveTrain extends Subsystem {
    private WPI_TalonSRX frontLeft;
    private WPI_TalonSRX frontRight;
    private WPI_TalonSRX backLeft;
    private WPI_TalonSRX backRight;

    private DifferentialDrive driveTrain;

    public DriveTrain() {
        frontLeft  = new WPI_TalonSRX(Constants.frontLeft);
        frontRight = new WPI_TalonSRX(Constants.frontRight);

        backLeft   = new WPI_TalonSRX(Constants.backLeft);
        backRight  = new WPI_TalonSRX(Constants.backRight);

        backLeft.set(ControlMode.Follower,Constants.frontLeft);
        backRight.set(ControlMode.Follower,Constants.frontRight);

        frontLeft.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative,0,0);
        frontRight.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative,0,0);
        

        frontLeft.configOpenloopRamp(.65,0);
        frontRight.configOpenloopRamp(.65,0);
        backRight.configOpenloopRamp(.65,0);
        backLeft.configOpenloopRamp(.65,0);

        frontLeft.configPeakOutputForward(1.0,0);
        frontLeft.configPeakOutputReverse(-1.0,0);
        frontRight.configPeakOutputForward(1.0,0);
        frontRight.configPeakOutputReverse(-1.0,0);
        backLeft.configPeakOutputForward(1.0,0);
        backLeft.configPeakOutputReverse(-1.0,0);
        backRight.configPeakOutputForward(1.0,0);
        backRight.configPeakOutputReverse(-1.0,0);

        frontLeft.setNeutralMode(NeutralMode.Brake);
        frontRight.setNeutralMode(NeutralMode.Brake);
        backLeft.setNeutralMode(NeutralMode.Brake);
        backRight.setNeutralMode(NeutralMode.Brake);

        frontLeft.set(ControlMode.PercentOutput, 0);
        frontRight.set(ControlMode.PercentOutput, 0);

        frontLeft.configNeutralDeadband(.03,0);
        frontRight.configNeutralDeadband(.03,0);

        driveTrain = new DifferentialDrive(frontLeft,frontRight);

        SmartDashboard.setDefaultNumber("Drive P",0);
        SmartDashboard.setDefaultNumber("Drive I",0);
        SmartDashboard.setDefaultNumber("Drive D", 0);

    }

    /**
     * Sets the default state of the robot to be driven by the joystick.
     */
    public void initDefaultCommand() {
        setDefaultCommand(new StickDrive());
    }

    public void zeroEncoders() {
        frontLeft.setSelectedSensorPosition(0,0,0);
        frontRight.setSelectedSensorPosition(0,0,0);
    }

    public WPI_TalonSRX getLeftTalon() {
        return frontLeft;
    }

    public WPI_TalonSRX getRightTalon() {
        return frontRight;
    }

    public double inchesToTicks(double inches){
        return (inches/(2*Math.PI*3)) * 4096;
    }


    public double getLeftEncoderValue(){
        return -frontLeft.getSensorCollection().getQuadraturePosition();
    }

    public double getRightEncoderValue(){
        return frontRight.getSensorCollection().getQuadraturePosition();
    }

    public void arcadeDrive(double forward, double turn) {
        driveTrain.arcadeDrive(forward, turn,true);
    }

    public void curvDrive(double forward, double rotation, boolean turn){
        driveTrain.curvatureDrive(forward,rotation,turn);

    }

}