package frc.team4192.robot.subsystems;


import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import edu.wpi.first.wpilibj.command.Subsystem;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import frc.team4192.robot.Constants;

import static java.lang.Math.abs;

public class Intake extends Subsystem {
    private TalonSRX intakeLeft;
    private TalonSRX intakeRight;
    private double  speed;
    private boolean speedControl;

    @Override
    protected void initDefaultCommand() {
    }

    public Intake() {
        intakeLeft = new TalonSRX(Constants.intakeLeft);
        intakeRight = new TalonSRX(Constants.intakeRight);

        intakeLeft.setNeutralMode(NeutralMode.Coast);
        intakeRight.setNeutralMode(NeutralMode.Coast);

        intakeRight.setInverted(true);
        intakeLeft.setInverted(false);

        intakeLeft.set(ControlMode.PercentOutput, 0);
        intakeRight.set(ControlMode.PercentOutput, 0);

        intakeLeft.configPeakOutputForward(1,0);
        intakeLeft.configPeakOutputReverse(-1,0);

        intakeRight.configPeakOutputForward(1,0);
        intakeRight.configPeakOutputReverse(-1,0);

        intakeLeft.configOpenloopRamp(0,0);
        intakeRight.configOpenloopRamp(0,0);
        speed = .7;
        speedControl = false;

        intakeLeft.configContinuousCurrentLimit(25,0);
        intakeLeft.configPeakCurrentLimit(30,0);
        intakeLeft.configPeakCurrentDuration(500,0);

        intakeRight.configContinuousCurrentLimit(25,0);
        intakeRight.configPeakCurrentLimit(30,0);
        intakeRight.configPeakCurrentDuration(500,0);

    }

    public void setBoth(double output1,double output2) {
        intakeLeft.set(ControlMode.PercentOutput, output1);
        intakeRight.set(ControlMode.PercentOutput, (output2));
    }

    public void motionMagic(int rotations) {
        intakeLeft.set(ControlMode.MotionMagic, rotations);
        intakeRight.set(ControlMode.MotionMagic, rotations);
    }

    public double getSpeed(){
        return speed;
    }

    public void setSpeed(double value){
        speed = value;
    }

    public void setControlBool(boolean val){
        speedControl = val;
    }

    public boolean getControlBool() {
        return speedControl;
    }


}
