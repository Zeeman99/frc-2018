package frc.team4192.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;

public class TipPneumatics extends Subsystem{
    private DoubleSolenoid doubleOne;
    private boolean position;
    //Extended = True

    public TipPneumatics() {
        doubleOne = new DoubleSolenoid(13, 4,5);
        position = false;
    }

    @Override
    protected void initDefaultCommand() {
    }

    public void extendSolenoid(){
        doubleOne.set(DoubleSolenoid.Value.kForward);
        position = true;
        Timer.delay(.05);
        stopSolenoid();

    }

    public void retractSolenoid(){
        doubleOne.set(DoubleSolenoid.Value.kReverse);
        position = false;
        Timer.delay(.05);
        stopSolenoid();
    }

    public boolean getPosition(){
        return position;
    }

    public void stopSolenoid(){
        doubleOne.set(DoubleSolenoid.Value.kOff);
    }
}
