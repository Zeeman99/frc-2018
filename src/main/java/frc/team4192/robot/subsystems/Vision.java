package frc.team4192.robot.subsystems;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.command.Subsystem;


public class Vision extends Subsystem {

    public Vision() {
        UsbCamera cam = CameraServer.getInstance().startAutomaticCapture(0);
        cam.setResolution(256,144);
        cam.setFPS(30);
    }

    @Override
    protected void initDefaultCommand() {
    }

}