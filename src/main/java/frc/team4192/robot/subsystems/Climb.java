package frc.team4192.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team4192.robot.Constants;

public class Climb extends Subsystem {

    private TalonSRX climb;

    public Climb(){
        climb = new TalonSRX(Constants.climb);
    }

    @Override
    protected void initDefaultCommand() {
    }

    public void setMotor(double value){
        climb.set(ControlMode.PercentOutput, value);
    }
}
