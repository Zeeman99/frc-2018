package frc.team4192.robot.subsystems;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team4192.robot.Constants;
import frc.team4192.robot.Robot;


public class Dashboard {

    public Dashboard(){
        Thread dashboardUpdateThread = new Thread(() -> {
            while (!Thread.interrupted()) {
                /*SmartDashboard.putNumber("Controller Y Axis",
                        Robot.oi.getYaxis());
                SmartDashboard.putNumber("Controller X Axis",
                        Robot.oi.getXaxis());*/
                SmartDashboard.putNumber("Gyro Angle",
                        Robot.gyro.getAngle());
                SmartDashboard.putNumber("DR4B Speed",
                        Robot.dr4b.getSpeed());
                /*SmartDashboard.putNumber("Drivetrain Speed",
                        Robot.speedControl.getSpeed());*/
                SmartDashboard.putNumber("Left Drive Encoder Ticks",
                        Robot.driveTrain.getLeftEncoderValue());
                SmartDashboard.putNumber("Right Drive Encoder Ticks",
                        Robot.driveTrain.getRightEncoderValue());
                SmartDashboard.putNumber("Right DR4B Encoder Ticks",
                        Robot.dr4b.getRightEncoderValue());
            }
        });
        dashboardUpdateThread.start();
    }
}
