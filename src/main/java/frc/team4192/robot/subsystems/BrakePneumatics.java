package frc.team4192.robot.subsystems;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;



public class BrakePneumatics extends Subsystem {
    private Compressor c;
    private DoubleSolenoid doubleOne;

    public BrakePneumatics() {
        doubleOne = new DoubleSolenoid(13, 0,1);
    }

    @Override
    protected void initDefaultCommand() {
    }

    public void turnOnCompressor(){
        c.setClosedLoopControl(true);
    }

    public void turnOffCompressor(){
        c.setClosedLoopControl(false);
    }

    public void disableBrake(){
        doubleOne.set(DoubleSolenoid.Value.kForward);

    }

    public void enableBrake(){
        doubleOne.set(DoubleSolenoid.Value.kReverse);
    }

    public void stopSolenoid(){
        doubleOne.set(DoubleSolenoid.Value.kOff);
    }



}