package frc.team4192.robot.subsystems;


import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import edu.wpi.first.wpilibj.command.Subsystem;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team4192.robot.Constants;

public class DR4B extends Subsystem {
    private double DR4Bspeed;

    @Override
    protected void initDefaultCommand() {
    }

    public DR4B() {
        Constants.liftLeft  = new TalonSRX(Constants.DR4BLeft);
        Constants.liftRight = new TalonSRX(Constants.DR4BRight);

        Constants.liftLeft.configOpenloopRamp(.5,0);
        Constants.liftRight.configOpenloopRamp(.5,0);

        Constants.liftLeft.setNeutralMode(NeutralMode.Brake);
        Constants.liftRight.setNeutralMode(NeutralMode.Brake);

        Constants.liftLeft.setInverted(true);

        Constants.liftRight.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative,0,0);
        Constants.liftRight.setSensorPhase(false);
        Constants.liftRight.setSelectedSensorPosition(0,0,0);


        Constants.liftLeft.set(ControlMode.PercentOutput, 0);
        Constants.liftRight.set(ControlMode.PercentOutput, 0);

        SmartDashboard.setDefaultNumber("DR4B Target",0);
        SmartDashboard.setDefaultNumber("DR4B P",0);
        SmartDashboard.setDefaultNumber("DR4B I",0);
        SmartDashboard.setDefaultNumber("DR4B D", 0);

        Constants.liftLeft.configContinuousCurrentLimit(30,0);
        Constants.liftLeft.configPeakCurrentLimit(40,0);
        Constants.liftLeft.configPeakCurrentDuration(500,0);

        Constants.liftRight.configContinuousCurrentLimit(30,0);
        Constants.liftRight.configPeakCurrentLimit(40,0);
        Constants.liftRight.configPeakCurrentDuration(500,0);

        DR4Bspeed = Constants.defaultLiftSpeed;


    }

    public void setBothSides(double output) {
        Constants.liftLeft.set(ControlMode.PercentOutput, output);
        Constants.liftRight.set(ControlMode.PercentOutput, output);
    }

    public void setPIDValues(double kP, double kI, double kD){
        Constants.liftRight.config_kP(0,kP,0);
        Constants.liftRight.config_kI(0,kI,0);
        Constants.liftRight.config_kD(0,kD,0);
    }

    public void setPIDPosition(double position){
        Constants.liftLeft.set(ControlMode.Follower, Constants.DR4BRight);
        Constants.liftRight.set(ControlMode.Position,position);
    }

    public double getError(){
        return Constants.liftRight.getClosedLoopError(0);
    }

    public void zeroEncoder(){
        Constants.liftRight.setSelectedSensorPosition(0,0,0);
    }

    public double getSpeed(){
        return DR4Bspeed;
    }

    public void setSpeed(double speed){
        DR4Bspeed = speed;
    }

    public int getRightEncoderValue() {
        return Constants.liftRight.getSensorCollection().getQuadraturePosition();
    }


}
