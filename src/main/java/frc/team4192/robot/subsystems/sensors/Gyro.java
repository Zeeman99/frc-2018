package frc.team4192.robot.subsystems.sensors;

import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team4192.robot.Constants;

public class Gyro {

    public Gyro(){
        Constants.gyro = new AHRS(SPI.Port.kMXP);
    }

    public void reset(){
        Constants.gyro.reset();
    }

    public double getAngle(){
        return Constants.gyro.getAngle();
    }

    public AHRS getGyro(){
        return Constants.gyro;
    }
}