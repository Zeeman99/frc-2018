package frc.team4192.robot.subsystems;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;


public class IntakePneumatics extends Subsystem {
    private DoubleSolenoid doubleOne;

    public IntakePneumatics() {
        doubleOne = new DoubleSolenoid(13, 2,3);
    }

    @Override
    protected void initDefaultCommand() {
    }

    public void extendSolenoid(){
        doubleOne.set(DoubleSolenoid.Value.kForward);

    }

    public void retractSolenoid(){
        doubleOne.set(DoubleSolenoid.Value.kReverse);
    }

    public void stopSolenoid(){
        doubleOne.set(DoubleSolenoid.Value.kOff);
    }



}