package frc.team4192.robot.subsystems;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team4192.robot.Constants;

public class SpeedControl extends Subsystem {

    private double speed = Constants.defaultDriveSensitivity;

    @Override
    protected void initDefaultCommand() {

    }

    public double getSpeed(){
        return speed;
    }

    public void setSpeed(double speed){
        this.speed = speed;
    }


}
