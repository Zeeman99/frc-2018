package frc.team4192.robot.triggers;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team4192.robot.Robot;


public class leftButtonPlusA extends Trigger{

    public leftButtonPlusA(){}

    public boolean get(){
        return (Robot.oi.getController().getRawButton(5) && Robot.oi.getController().getRawButton(1));
    }

}
