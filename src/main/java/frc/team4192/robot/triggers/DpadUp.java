package frc.team4192.robot.triggers;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team4192.robot.Robot;

public class DpadUp extends Trigger {

    DpadUp(){}

    public boolean get(){
        return (Robot.oi.getController().getPOV() == 0);
    }
}
