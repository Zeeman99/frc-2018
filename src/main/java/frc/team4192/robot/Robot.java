package frc.team4192.robot;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team4192.robot.commands.commandGroups.*;
import frc.team4192.robot.subsystems.*;
import frc.team4192.robot.subsystems.sensors.Gyro;

public class Robot extends IterativeRobot {

    public static DriveTrain driveTrain;
    public static Gyro gyro;
    public static DR4B dr4b;
    public static Intake intake;
    public static IntakePneumatics intakePneumatics;
    public static BrakePneumatics brakePneumatics;
    public static SpeedControl speedControl;
    public static Climb climb;
    public static Vision vision;
    public static OI oi;
    //private       Dashboard dashboard;

    private SendableChooser<String> autoChooser;
    private SendableChooser<String> positionChooser;
    private CommandGroup autonomousCommandGroup;

    @Override
    public void robotInit() {
        driveTrain = new DriveTrain();
        gyro = new Gyro();
        dr4b = new DR4B();
        intake = new Intake();
        intakePneumatics = new IntakePneumatics();
        brakePneumatics  = new BrakePneumatics();
        speedControl = new SpeedControl();
        oi = new OI();
        vision = new Vision();
        //dashboard = new Dashboard();

        autonomousCommandGroup = null;
        autoChooser = new SendableChooser<>();
        autoChooser.addDefault("Baseline", "Baseline");
        autoChooser.addObject("Switch", "Switch");
        autoChooser.addObject("Scale","Scale");

        positionChooser = new SendableChooser<>();
        positionChooser.addDefault("Left","L");
        positionChooser.addObject("Middle","M");
        positionChooser.addObject("Right","R");


        SmartDashboard.putData("Autonomous Selector", autoChooser);
        SmartDashboard.putData("Position Selector",positionChooser);

        SmartDashboard.setDefaultNumber("Autonomous Delay",0);
        SmartDashboard.setDefaultBoolean("Fully On Side",false);
    }

    @Override
    public void disabledInit() {
        super.disabledInit();
    }

    @Override
    public void disabledPeriodic() {
    }

    @Override
    public void autonomousInit() {
        Robot.driveTrain.zeroEncoders();
        Robot.dr4b.zeroEncoder();
        int retries = 100;
        String gameData = DriverStation.getInstance().getGameSpecificMessage();
        while (gameData.length() < 2 && retries > 0) {
            retries--;
            try {
                Thread.sleep(5);
            } catch (InterruptedException ie) {
                // Ignore interrupted exception
            }
            gameData = DriverStation.getInstance().getGameSpecificMessage();
        }

        char positionSelection = positionChooser.getSelected().charAt(0);
        char gameDataSwitch = gameData.charAt(0);
        char gameDataScale  = gameData.charAt(1);
        String autoSelection = autoChooser.getSelected();


        autonomousCommandGroup = new Baseline(SmartDashboard.getNumber("Autonomous Delay",0));

        if(positionSelection == gameDataSwitch && autoSelection.equals("Switch")) {
            autonomousCommandGroup = new SwitchStraight();
        }
        if(autoSelection.equals("Switch") && positionSelection == gameDataSwitch && SmartDashboard.getBoolean("Fully On Side",false)){
            if(positionSelection == 'R')
                    autonomousCommandGroup = new RightSwitchSide();
            if(positionSelection == 'L')
                autonomousCommandGroup = new LeftSwitchSide();
        }
        else if (autoSelection.equals("Switch") && positionSelection == 'M'){
            if(gameDataSwitch == 'R')
                autonomousCommandGroup = new RightSwitchMiddle();
            else
                autonomousCommandGroup = new LeftSwitchMiddle();
        }
        else if (autoSelection.equals("Scale") && positionSelection != gameDataScale && positionSelection == 'R')
            autonomousCommandGroup = new CrossScaleRightToLeft();
        else if (autoSelection.equals("Scale") && positionSelection != gameDataScale && positionSelection == 'L')
            autonomousCommandGroup = new CrossScaleLeftToRight();
        else if (autoSelection.equals("Scale") && positionSelection == gameDataScale)
            autonomousCommandGroup = new ScaleAuton(gameDataScale);
        
        autonomousCommandGroup.start();
    }

    @Override
    public void autonomousPeriodic() {
        Scheduler.getInstance().run();
    }

    @Override
    public void teleopInit() {
        if (autonomousCommandGroup != null) {
            autonomousCommandGroup.cancel();
        }
    }

    @Override
    public void teleopPeriodic() {
        Scheduler.getInstance().run();
    }
}