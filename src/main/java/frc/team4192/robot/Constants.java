package frc.team4192.robot;

import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.kauailabs.navx.frc.AHRS;

public class Constants {
    //Motor Controllers
    //Drive
    public static int frontRight = 10;
    public static int backRight  = 9;
    public static int frontLeft  = 1;
    public static int backLeft   = 2;

    public static double defaultDriveSensitivity = .75;
    public static double defaultLiftSpeed  = .9;

    public static int climb = 6;

    //Intake
    public static int intakeLeft  = 4;
    public static int intakeRight = 7;


    //DR4B
    public static int DR4BLeft  = 5;
    public static int DR4BRight = 8;

    public static TalonSRX liftLeft;
    public static TalonSRX liftRight;

    public static int scaleHeight = 4400;
    public static int switchHeight = 1500;
    public static int groundHeight = 200;

    //Gyro
    public static AHRS gyro;

}
