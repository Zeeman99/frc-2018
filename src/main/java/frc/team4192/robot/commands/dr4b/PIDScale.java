package frc.team4192.robot.commands.dr4b;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team4192.robot.Robot;

public class PIDScale extends Command {
    public PIDScale() {
    requires(Robot.dr4b);
    requires(Robot.brakePneumatics);
    }

    @Override
    protected void initialize() {
        System.out.println("Running PID DR4B");
        super.initialize();
        Robot.brakePneumatics.disableBrake();
        Robot.dr4b.setPIDValues(SmartDashboard.getNumber("DR4B P",0),SmartDashboard.getNumber("DR4B I",0),SmartDashboard.getNumber("DR4B D",0));
    }

    @Override
    protected void execute() {
        Robot.dr4b.setPIDPosition(SmartDashboard.getNumber("DR4B Target",0));
        SmartDashboard.putNumber("DR4B Error",Robot.dr4b.getError());
    }

    @Override
    protected void end() {
        Robot.brakePneumatics.enableBrake();
        Robot.dr4b.setBothSides(0);
    }

    @Override
    protected void interrupted() {
        super.interrupted();
    }

    @Override
    protected boolean isFinished() {
        return Math.abs(Robot.dr4b.getRightEncoderValue()-SmartDashboard.getNumber("DR4B Target",0)) < 200;
    }
}
