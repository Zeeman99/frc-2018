package frc.team4192.robot.commands.dr4b;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class ResetEncoder extends Command {

    public ResetEncoder() {
    }

    @Override
    protected void initialize() {
        super.initialize();
    }

    @Override
    protected void execute() {
        Robot.dr4b.zeroEncoder();
    }

    @Override
    protected void end() {
    }

    @Override
    protected boolean isFinished() {
        return true;
    }

}


