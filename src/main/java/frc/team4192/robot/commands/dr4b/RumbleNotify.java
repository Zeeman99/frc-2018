package frc.team4192.robot.commands.dr4b;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;
import frc.team4192.robot.Constants;

public class RumbleNotify extends Command{

    public RumbleNotify(){}

    @Override
    protected void initialize() {super.initialize();}

    @Override
    protected void execute() {
        if(Constants.liftRight.getSensorCollection().getQuadraturePosition() >= Constants.scaleHeight) {
            Robot.oi.getController().setRumble(GenericHID.RumbleType.kLeftRumble, 1);
            Robot.oi.getController().setRumble(GenericHID.RumbleType.kRightRumble, 1);
        }
    }

    @Override
    protected void end() {
        Robot.oi.getController().setRumble(GenericHID.RumbleType.kLeftRumble, 1);
        Robot.oi.getController().setRumble(GenericHID.RumbleType.kRightRumble, 1);
    }

    @Override
    protected boolean isFinished() {
        return Constants.liftRight.getSensorCollection().getQuadraturePosition() < Constants.scaleHeight;
    }

}
