package frc.team4192.robot.commands.dr4b;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Constants;
import frc.team4192.robot.Robot;

public class LiftSensitivity extends Command {
    private double sensitivity;

    public LiftSensitivity(double sensitivity){
        this.sensitivity = sensitivity;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected void execute(){
        Robot.dr4b.setSpeed(sensitivity);
    }

    @Override
    protected void end(){
        Robot.dr4b.setSpeed(Constants.defaultLiftSpeed);
    }

    @Override
    protected void interrupted() {
        super.interrupted();
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
