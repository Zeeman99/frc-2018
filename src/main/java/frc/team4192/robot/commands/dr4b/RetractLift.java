package frc.team4192.robot.commands.dr4b;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class RetractLift extends Command{

    public RetractLift(){
        requires(Robot.dr4b);
        requires(Robot.brakePneumatics);}

    @Override
    protected void initialize() {
        super.initialize();
        Robot.brakePneumatics.disableBrake();}

    @Override
    protected void execute() {
        Robot.dr4b.setBothSides(.75 * -Robot.dr4b.getSpeed());
    }

    @Override
    protected void end() {
        Robot.dr4b.setBothSides(0);
        Robot.brakePneumatics.enableBrake();
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
