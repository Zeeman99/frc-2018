package frc.team4192.robot.commands.auto;

public class PID
{
    double P;
    double I;
    double D;
    double target;

    final double MAX_INTEGRAL_VALUE = .5;
    public double sumError;

    public PID(double P, double I, double D, double inches)
    {
        this.P = P;
        this.I = I;
        this.D = D;

        sumError = 0;
        target = (inches/(6 * Math.PI)) * 4096;
    }

    public double calculateVelocity(double encoderTicks)
    {
        double error = target-encoderTicks;
        double INTEGRAL_ACTIVE_ZONE = target * 0.125;

        if (Math.abs((I * sumError)) > MAX_INTEGRAL_VALUE)
        {
            if (sumError < 0)
            {
                sumError = -MAX_INTEGRAL_VALUE / I;
            }
            else
            {
                sumError = MAX_INTEGRAL_VALUE / I;
            }
        }

        if (error <= INTEGRAL_ACTIVE_ZONE)
        {
            sumError += error;
        }
        else
        {
            sumError = 0;
        }

        return P * error + I * sumError;
    }

    public double getDistanceAway(double encoderTicks){
        return (target-encoderTicks)/target;
    }


}
