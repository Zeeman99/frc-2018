package frc.team4192.robot.commands.auto;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class IntakeCubeAuto extends Command {

    public IntakeCubeAuto(){
        requires(Robot.intake);
        setTimeout(5);
    }

    public IntakeCubeAuto(double timeout){
        requires(Robot.intake);
        setTimeout(timeout);
    }

    @Override
    protected void initialize() {
    }

    @Override
    protected void execute() {
        Robot.intake.setBoth(1,1);
    }

    @Override
    protected void end() {
        Robot.intake.setBoth(0,0);
    }

    @Override
    protected boolean isFinished() {
        return isTimedOut();
    }
}
