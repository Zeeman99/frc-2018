package frc.team4192.robot.commands.auto;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class CustomDriveDistance extends Command {

    PID pid;

    @Override
    protected void initialize() {
        pid = new PID(.01,0,0,48);
    }

    @Override
    protected void execute() {
        double forward = pid.calculateVelocity(Robot.driveTrain.getRightEncoderValue());
    }

    @Override
    protected void end() {
        super.end();
    }

    @Override
    protected boolean isFinished() {
        return pid.getDistanceAway(Robot.driveTrain.getLeftEncoderValue()) < .03;

    }
}
