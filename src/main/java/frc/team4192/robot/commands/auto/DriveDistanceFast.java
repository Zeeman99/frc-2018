package frc.team4192.robot.commands.auto;

import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import frc.team4192.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * A command to drive to a specified distance in inches using the drive encoders.
 */
public class DriveDistanceFast extends Command {
    private PIDController pid;
    private PIDController pidTurn;
    private double forward;
    private double turn;


    public DriveDistanceFast(double distance, double angle){
        requires(Robot.driveTrain);

        forward = 0;
        turn = 0;

        distance = Robot.driveTrain.inchesToTicks(distance);

        pid = new PIDController(.00009, 0, 0, new pidSource(), new pidOutput());
        pidTurn = new PIDController(.049, 0.0, 0, Robot.gyro.getGyro(), new pidOutputTurn());
        if(distance>0)
        {
            pid.setInputRange(0,distance+10);
        }else
        {
            pid.setInputRange(distance-10,0);
        }
        pid.setAbsoluteTolerance(3000);
        pid.setOutputRange(-0.8,0.8);

        pid.setSetpoint(distance);

        pidTurn.setOutputRange(-0.50,0.50);
        pidTurn.setAbsoluteTolerance(2);
        pidTurn.setInputRange(-180,180);
        pidTurn.setSetpoint(0);
        pidTurn.setContinuous(true);
        pidTurn.setSetpoint(angle);
    }
    @Override
    protected void initialize() {
        Robot.driveTrain.zeroEncoders();
        Robot.gyro.reset();
        pid.enable();
        pidTurn.enable();
    }

    @Override
    protected void execute() {
        Robot.driveTrain.arcadeDrive(forward,turn);
    }

    /**
     *
     * @return when pid is on target or we are about to hit something
     */
    @Override
    protected boolean isFinished() {
        return pid.onTarget();
    }

    @Override
    protected void end() {
        pidTurn.disable();
        pidTurn.reset();
        pid.disable();
        pid.reset();
        Robot.driveTrain.arcadeDrive(0,0);
    }

    @Override
    protected void interrupted() {
        end();
    }

    private class pidOutput implements PIDOutput {
        @Override
        public void pidWrite(double output) {
            forward=output;
        }
    }
    private class pidOutputTurn implements PIDOutput {
        @Override
        public void pidWrite(double output) {
            turn=output;
        }
    }

    private class pidSource implements PIDSource {
        @Override
        public double pidGet() {
            return Robot.driveTrain.getLeftEncoderValue();
        }

        @Override
        public void setPIDSourceType(PIDSourceType pidSource){}

        @Override
        public PIDSourceType getPIDSourceType() {
            return PIDSourceType.kDisplacement;
        }
    }
}
