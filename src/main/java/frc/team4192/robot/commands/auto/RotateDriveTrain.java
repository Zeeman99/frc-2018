package frc.team4192.robot.commands.auto;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team4192.robot.Robot;


/**
 * This command rotates the drive train to a given degree.
 */
public class RotateDriveTrain extends Command{
    private PIDController pid;
    private double turnOutput;

    public RotateDriveTrain(double theta){
        requires(Robot.driveTrain);
        turnOutput = 0;

        pid = new PIDController(.016 ,0, 0, Robot.gyro.getGyro(), new pidOutput());
        pid.setAbsoluteTolerance(2);
        pid.setInputRange(-180,180);
        pid.setOutputRange(-0.6,0.6);
        pid.setSetpoint(theta);
        pid.setContinuous(true);
        SmartDashboard.putNumber("Auto Rotation Target", theta);
    }

    @Override
    protected void initialize() {
        Robot.gyro.reset();
        pid.enable();
    }

    @Override
    protected void execute() {
        Robot.driveTrain.arcadeDrive(0, turnOutput);
    }

    @Override
    protected boolean isFinished() {
        return pid.onTarget();
    }

    @Override
    protected void end() {
        Robot.driveTrain.zeroEncoders();
        Robot.driveTrain.arcadeDrive(0,0);
        pid.disable();
        pid.reset();
    }

    private class pidOutput implements PIDOutput {
            @Override
            public void pidWrite(double output) {
                turnOutput = output;
            }
    }

}