package frc.team4192.robot.commands.auto;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class OutakeCubeAuto extends Command {

    public OutakeCubeAuto(){
        requires(Robot.intake);
        setTimeout(.5);
    }

    @Override
    protected void initialize() {
    }

    @Override
    protected void execute() {
        Robot.intake.setBoth(-.3,-.3);
    }

    @Override
    protected void end() {
        Robot.intake.setBoth(0,0);
    }

    @Override
    protected boolean isFinished() {
        return isTimedOut();
    }
}
