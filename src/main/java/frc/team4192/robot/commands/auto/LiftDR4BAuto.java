package frc.team4192.robot.commands.auto;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class LiftDR4BAuto extends Command {

    private int target;
    public LiftDR4BAuto(int ticks){
        requires(Robot.dr4b);
        requires(Robot.brakePneumatics);
        target = ticks;
        setTimeout(1.5);
    }
    public LiftDR4BAuto(int ticks, double timeout){
        requires(Robot.dr4b);
        requires(Robot.brakePneumatics);
        target = ticks;
        setTimeout(timeout);
    }

    @Override
    protected void initialize() {
        Robot.brakePneumatics.disableBrake();
    }

    @Override
    protected void execute() {
        Robot.dr4b.setBothSides(.9);
    }

    @Override
    protected void end() {
        Robot.dr4b.setBothSides(0);
        Robot.brakePneumatics.enableBrake();
    }

    @Override
    protected boolean isFinished() {
        return isTimedOut() || Robot.dr4b.getRightEncoderValue() > target;
    }
}