package frc.team4192.robot.commands.auto;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;


public class TimeBasedTurn extends Command {

    private Timer timer;

    public TimeBasedTurn(){
        requires(Robot.driveTrain);
        timer = new Timer();
    }

    @Override
    protected void initialize() {
        timer.start();
    }

    @Override
    protected void execute() {
        Robot.driveTrain.arcadeDrive(.1,-.5);
    }

    @Override
    protected void end() {
        Robot.driveTrain.arcadeDrive(0,0);
    }

    @Override
    protected boolean isFinished() {
        return timer.hasPeriodPassed(1.3);
    }
}
