package frc.team4192.robot.commands.pnuematics;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class CloseBrake extends Command {

    public CloseBrake(){
        requires(Robot.brakePneumatics);
    }

    @Override
    protected void initialize() {
        super.initialize();
    }

    @Override
    protected void execute() {
        Robot.brakePneumatics.enableBrake();
    }

    @Override
    protected void end() {
        Robot.brakePneumatics.stopSolenoid();
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
