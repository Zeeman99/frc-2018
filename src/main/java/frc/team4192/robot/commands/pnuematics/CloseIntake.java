package frc.team4192.robot.commands.pnuematics;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class CloseIntake extends Command {
    public CloseIntake(){
        requires(Robot.intakePneumatics);
    }

    @Override
    protected void initialize() {
        super.initialize();
    }

    @Override
    protected void execute() {
        Robot.intakePneumatics.retractSolenoid();
    }

    @Override
    protected void end() {
        Robot.intakePneumatics.stopSolenoid();
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
