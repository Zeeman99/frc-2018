package frc.team4192.robot.commands.pnuematics;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class OpenBrake extends Command {

    public OpenBrake(){
        requires(Robot.brakePneumatics);
    }

    @Override
    protected void initialize() {
        super.initialize();
    }

    @Override
    protected void execute() {
        Robot.brakePneumatics.disableBrake();
    }

    @Override
    protected void end() {
        Robot.brakePneumatics.stopSolenoid();
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
