package frc.team4192.robot.commands.drive;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Constants;
import frc.team4192.robot.Robot;
import frc.team4192.robot.subsystems.SpeedControl;

public class MidSpeed extends Command {

    public MidSpeed(){
        requires(Robot.speedControl);
    }

    @Override
    protected void initialize() {

    }
    @Override
    protected void execute(){
        Robot.speedControl.setSpeed(.45);
    }

    @Override
    protected void end(){
        Robot.speedControl.setSpeed(Constants.defaultDriveSensitivity);
    }

    @Override
    protected void interrupted() {
        super.interrupted();
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
