package frc.team4192.robot.commands.drive;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Constants;
import frc.team4192.robot.Robot;

/**
 * Default command to drive the robot with joysticks. It may be overridden during autonomous mode to run drive sequence. This command grabs
 * the left and right joysticks on an XBox controller and sets them to arcade drive.
 */
public class StickDrive extends Command {

    public StickDrive() {
        requires(Robot.driveTrain);
    }

    protected void initialize() {
    }

    protected void execute() {

        double driveSensitivity = Robot.speedControl.getSpeed();

        if(Constants.liftRight.getSelectedSensorPosition(0) > 2000)
            driveSensitivity = .3;

        //Robot.driveTrain.arcadeDrive(-Robot.oi.getYaxis() * driveSensitivity, Robot.oi.getXaxis() * driveSensitivity);
        Robot.driveTrain.curvDrive(-Robot.oi.getYaxis() * driveSensitivity, Robot.oi.getXaxis() * driveSensitivity,true);
    }

    protected boolean isFinished() {
        return false;
    }

    protected void end() {
        Robot.driveTrain.arcadeDrive(0, 0);
    }

    protected void interrupted() {

    }
}