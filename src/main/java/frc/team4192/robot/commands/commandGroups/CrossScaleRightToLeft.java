package frc.team4192.robot.commands.commandGroups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;
import frc.team4192.robot.Constants;
import frc.team4192.robot.commands.auto.DriveDistanceFast;
import frc.team4192.robot.commands.auto.LiftDR4BAuto;
import frc.team4192.robot.commands.auto.OutakeCubeAuto;
import frc.team4192.robot.commands.auto.RotateDriveTrain;

public class CrossScaleRightToLeft extends CommandGroup{
    public CrossScaleRightToLeft(){
        addSequential(new DriveDistanceFast(225,0));
        addSequential(new WaitCommand(.2));
        addSequential(new RotateDriveTrain(-90));
        addSequential(new WaitCommand(.2));
        addSequential(new DriveDistanceFast(250,0));
        addSequential(new WaitCommand(.2));
        addSequential(new RotateDriveTrain(90));
        addSequential(new DriveDistanceFast(80,0));
        addSequential(new RotateDriveTrain(90));
        //addSequential(new DriveDistanceFast(10,0));
        addSequential(new LiftDR4BAuto(800));
        addSequential(new WaitCommand(.75));
        addSequential(new LiftDR4BAuto(Constants.scaleHeight));
        addSequential(new OutakeCubeAuto());

    }

}
