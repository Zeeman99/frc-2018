package frc.team4192.robot.commands.commandGroups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;
import frc.team4192.robot.commands.auto.*;

public class ScaleAuton extends CommandGroup {
    public ScaleAuton(char position){
        addSequential(new DriveDistanceFast(300,0));
        if(position == 'R')
            addSequential(new RotateDriveTrain(-100),2);
        else
            addSequential(new RotateDriveTrain(100),2);
        addSequential(new WaitCommand(.25));
        addSequential(new DriveDistance(30,0),1);
        addSequential(new LiftDR4BAuto(800));
        addSequential(new WaitCommand(.5));
        addSequential(new LiftDR4BAuto(5300));
        addSequential(new OutakeCubeAuto());

    }
}
