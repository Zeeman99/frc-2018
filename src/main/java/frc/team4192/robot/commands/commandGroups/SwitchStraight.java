package frc.team4192.robot.commands.commandGroups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;
import frc.team4192.robot.Constants;
import frc.team4192.robot.commands.auto.DriveDistance;
import frc.team4192.robot.commands.auto.LiftDR4BAuto;
import frc.team4192.robot.commands.auto.OutakeCubeAuto;

public class SwitchStraight extends CommandGroup {
    public SwitchStraight(){
        addSequential(new DriveDistance(120,0),5);
        addSequential(new LiftDR4BAuto(Constants.switchHeight));
        addSequential(new OutakeCubeAuto());
    }
}
