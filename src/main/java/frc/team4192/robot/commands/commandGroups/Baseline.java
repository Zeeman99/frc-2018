package frc.team4192.robot.commands.commandGroups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;
import frc.team4192.robot.Robot;
import frc.team4192.robot.commands.auto.*;

/*
  Auto Mode For Passing the Baseline
  1. Drive Forward ___ Inches
*/

public class Baseline extends CommandGroup {

    public Baseline(double delay){
        addSequential(new WaitCommand(delay));
        addSequential(new DriveDistanceFast(114,0));
    }
}
