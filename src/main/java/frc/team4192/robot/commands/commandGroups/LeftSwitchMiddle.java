package frc.team4192.robot.commands.commandGroups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;
import frc.team4192.robot.Constants;
import frc.team4192.robot.commands.auto.*;

public class LeftSwitchMiddle extends CommandGroup{

    public LeftSwitchMiddle(){
        addSequential(new DriveDistance(40,0));
        addSequential(new RotateDriveTrain(-55),1);
        addSequential(new WaitCommand(.1));
        addSequential(new DriveDistance(70,0));
        addSequential(new RotateDriveTrain(60),1);
        addSequential(new WaitCommand(.1));
        addSequential(new DriveDistance(70,0));
        addSequential(new DriveDistance(-25,0),.2);
        addSequential(new LiftDR4BAuto(Constants.switchHeight));
        addSequential(new OutakeCubeAuto());
    }
}
