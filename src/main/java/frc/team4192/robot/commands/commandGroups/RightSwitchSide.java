package frc.team4192.robot.commands.commandGroups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;
import frc.team4192.robot.Constants;
import frc.team4192.robot.Robot;
import frc.team4192.robot.commands.auto.*;

public class RightSwitchSide extends CommandGroup {

    public RightSwitchSide(){
        addSequential(new DriveDistance(154,0));
        addSequential(new RotateDriveTrain(-90),1.5);
        addSequential(new WaitCommand(.1));
        addSequential(new DriveDistance(30,0),1);
        addSequential(new LiftDR4BAuto(Constants.switchHeight));
        addSequential(new OutakeCubeAuto());
    }
}
