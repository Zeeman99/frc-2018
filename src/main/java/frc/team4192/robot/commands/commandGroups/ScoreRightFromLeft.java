package frc.team4192.robot.commands.commandGroups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.team4192.robot.commands.auto.DriveDistance;
import frc.team4192.robot.commands.auto.RotateDriveTrain;

public class ScoreRightFromLeft extends CommandGroup {
    public ScoreRightFromLeft(){
        addSequential(new DriveDistance(60,0));
        addSequential(new RotateDriveTrain(90));
        addSequential(new DriveDistance(172,0));
        addSequential(new RotateDriveTrain(-90));
        addSequential(new DriveDistance(75,0));
    }
}
