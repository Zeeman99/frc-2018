package frc.team4192.robot.commands.commandGroups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;
import frc.team4192.robot.Constants;
import frc.team4192.robot.commands.auto.*;
import frc.team4192.robot.commands.pnuematics.CloseIntake;
import frc.team4192.robot.commands.pnuematics.OpenIntake;

public class TwoCubeMiddleRight extends CommandGroup{
    public TwoCubeMiddleRight(){
        addSequential(new DriveDistanceFast(40,0));
        addSequential(new RotateDriveTrain(55),.75);
        addSequential(new WaitCommand(.1));
        addSequential(new DriveDistanceFast(55 ,0));
        addSequential(new RotateDriveTrain(-50),.75);
        addSequential(new WaitCommand(.1));
        addSequential(new DriveDistanceFast(60,0),.75);
        addSequential(new LiftDR4BAuto(Constants.switchHeight),.75);
        addSequential(new OutakeCubeAuto());
        addParallel(  new DropDR4BAuto(400,.4));
        addSequential(new DriveDistanceFast(-60,0));
        addSequential(new RotateDriveTrain(-55),.75);
        addParallel(  new IntakeCubeAuto());
        addSequential(new DriveDistance(90,0),2);
        addSequential(new DriveDistanceFast(-50,0));
        addSequential(new WaitCommand(.05));
        addSequential(new RotateDriveTrain(65),.75);
        addSequential(new DriveDistanceFast(80,0));
        addSequential(new LiftDR4BAuto(Constants.switchHeight),.75);
        addSequential(new OutakeCubeAuto());
    }
}
