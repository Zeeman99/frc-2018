package frc.team4192.robot.commands.commandGroups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;
import frc.team4192.robot.Constants;
import frc.team4192.robot.Robot;
import frc.team4192.robot.commands.auto.*;

public class LeftSwitchSide extends CommandGroup {

    public LeftSwitchSide(){
        addSequential(new DriveDistanceFast(154,0));
        addSequential(new RotateDriveTrain(95),2);
        addSequential(new WaitCommand(.1));
        addSequential(new DriveDistance(50,0),2);
        addSequential(new LiftDR4BAuto(Constants.switchHeight));
        addSequential(new OutakeCubeAuto());
    }
}
