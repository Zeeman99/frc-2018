package frc.team4192.robot.commands.climb;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class ClimbUpwards extends Command{
    public ClimbUpwards()
    { requires(Robot.climb);requires(Robot.brakePneumatics);
    }

    @Override
    protected void initialize() {
        super.initialize();
        Robot.brakePneumatics.disableBrake();
    }

    @Override
    protected void execute() {
        Robot.climb.setMotor(1);
    }

    @Override
    protected void end() {
        Robot.climb.setMotor(0);
        Robot.brakePneumatics.enableBrake();
    }

    @Override
    protected boolean isFinished() {
        return false;
    }

}
