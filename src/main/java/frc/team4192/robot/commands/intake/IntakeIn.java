package frc.team4192.robot.commands.intake;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Robot;

public class IntakeIn extends Command{
    private double speed;

    public IntakeIn(double val){
        requires(Robot.intake);
        speed = val;
    }

    @Override
    protected void initialize() {super.initialize();}

    @Override
    protected void execute() {
        Robot.intake.setBoth(speed,speed);
    }

    @Override
    protected void end() {
        Robot.intake.setBoth(0,0);
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
