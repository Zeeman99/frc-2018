package frc.team4192.robot.commands.intake;

import edu.wpi.first.wpilibj.command.Command;
import frc.team4192.robot.Constants;
import frc.team4192.robot.Robot;

public class IntakeSpeedControl extends Command{

    public IntakeSpeedControl(){}

    @Override
    protected void initialize() {super.initialize();}

    @Override
    protected void execute() {
        Robot.intake.setSpeed(.55);
        Robot.intake.setControlBool(true);
    }

    @Override
    protected void end() {
        Robot.intake.setSpeed(.7);
        Robot.intake.setControlBool(false);
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
