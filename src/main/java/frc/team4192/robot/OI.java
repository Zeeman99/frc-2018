package frc.team4192.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team4192.robot.commands.climb.ClimbDownwards;
import frc.team4192.robot.commands.climb.ClimbUpwards;
import frc.team4192.robot.commands.dr4b.ElevateLift;
import frc.team4192.robot.commands.dr4b.LiftSensitivity;
import frc.team4192.robot.commands.dr4b.ResetEncoder;
import frc.team4192.robot.commands.dr4b.RetractLift;
import frc.team4192.robot.commands.drive.MidSpeed;
import frc.team4192.robot.commands.intake.IntakeIn;
import frc.team4192.robot.commands.intake.IntakeOut;
import frc.team4192.robot.commands.intake.IntakeSpeedControl;
import frc.team4192.robot.commands.pnuematics.*;
import frc.team4192.robot.commands.drive.LowestSpeed;

public class OI {
    private Joystick controller = new Joystick(0);
    private Joystick driverStick = new Joystick(1);

    public OI() {
        JoystickButton driverX = new JoystickButton(driverStick, 3);
        JoystickButton driverY = new JoystickButton(driverStick, 4);

        JoystickButton x = new JoystickButton(controller, 3);
        JoystickButton y = new JoystickButton(controller, 4);
        JoystickButton a = new JoystickButton(controller, 1);
        JoystickButton b = new JoystickButton(controller, 2);
        JoystickButton rb = new JoystickButton(controller, 6);
        JoystickButton lb = new JoystickButton(controller, 5);
        JoystickButton back = new JoystickButton(controller, 7);
        JoystickButton start = new JoystickButton(controller, 8);
        Trigger dpadUp = new Trigger() {
            @Override
            public boolean get() {
                return getController().getPOV() == 0;
            }
        };
        Trigger dpadDown = new Trigger() {
            @Override
            public boolean get() {
                return getController().getPOV() == 180;
            }
        };

        Trigger dpadLeft = new Trigger() {
            @Override
            public boolean get() {
                return getController().getPOV() == 270;
            }
        };
        Trigger leftTrigger = new Trigger() {
            @Override
            public boolean get() {
                return getController().getRawAxis(2) > .05;
            }
        };
        Trigger rightTrigger = new Trigger() {
            @Override
            public boolean get() {
                return getController().getRawAxis(3) > .05;
            }
        };

        driverX.toggleWhenPressed(new MidSpeed());
        driverY.toggleWhenPressed(new LowestSpeed());

        a.whileHeld(new ElevateLift());
        b.whileHeld(new RetractLift());

        start.toggleWhenPressed(new LiftSensitivity(.4));

        y.whileHeld(new OpenIntake());
        x.whileHeld(new CloseIntake());

        lb.whileHeld(new IntakeIn(.65));
        leftTrigger.whileActive(new IntakeIn(1));

        rb.whileHeld(new IntakeOut(.3));
        rightTrigger.whileActive(new IntakeOut(.55));


    }

    public Joystick getController() {
        return controller;
    }

    public double getXaxis() {
        return driverStick.getRawAxis(4);
    }

    public double getYaxis() {
        return driverStick.getY();
    }
}